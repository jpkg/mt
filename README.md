# mt

This is an excessively complex set of perl scripts designed for automatically
picking meetings.

# Installation

You must install TOML from CPAN in order for the program to work.

1. Clone this repository.
2. `cd mt && ./mt --gen` (this will generate all of the configurations)
3. A prompt should show up with further instructions on configuration depending
   on your shell.

# Configuration

When `mt` is first run, it will automatically write a default configuration
file to `$HOME/.config/mt/config.toml`.

It is recommended to use `mt --edit` to edit the configuration, as it will also
automatically run checks to prevent syntax errors.

## Changing Configuration Editor.

Go to /env/env.sh (as written by the build.pl script) and change the
`MT_EDITOR` variable as desired.

## Configuring Meetings

A Meeting may have a url, weekday-specific urls, and a set of aliases.  Note
that days of the week must be capitalized and written out in full.

```toml 
[ Meeting ] 
url = "url1.com" 
# Use the below URL for Mondays only (overrides the default url)
Monday = "MondayURL.com" 
# aliases 
aliases = ['m', 'mng']
```
The aliases allow you to access the meeting without having to write out the
full name of the meeting. For instance, in our example above, if you were to
write `mt m`, mt should also bring you to the appropriate url, just as if you
wrote `mt Meeting`.

The url variable does not need to be included if you do not need it as a
default.

## Configuring Meetings to join at specific times of the week
In order to have a meeting scheduled for a specific time of the week, place its
time and name under the corresponding table as such:

```toml
[ Monday ]
"9:30AM" = "Meeting1"
"11AM" = "Meeting2"
"2:30PM" = "Meeting3"
```

Note that **you cannot use aliases here**, as they are only for CLI use. The
names must be exact in capitalization and spelling to the ones in the table.

mt supports two different time representations, "hour:minute(AM|PM)" and
"hour(AM|PM)". If you wish to understand how mt parses this, see the @tr array
of regular expressions in lib/Meetings.pm.

## Configuring Browser
If you are on a UNIX-like operating system, just set the browser to whatever
browser binary you have.

### On macOS 
Since macOS does not have binaries but rather uses Applications,
you must set your browser variable in the Config table to the name of the
browser. If the name is invalid, mt should fall back to the default browser.

# Checking for errors/possible errors in configuration file.

Run `mt --check`

#!/usr/bin/env perl

package Check;

use strict;
use warnings;
use diagnostics;
use File::Basename;
use lib dirname (__FILE__);
use Messages;
use Meeting;
use Data::Dumper;
use feature 'say';

# takes in text and pattern and sees if they match.
sub _caps {
    my ($t, $p) = @_;
    my $re = '';
    for my $c (split (//, $p)) {
        my $x;
        if ($c =~ /[a-z]/) {
            $x = chr(ord($c) - 32);
        } else {
            $x = chr(ord($c) + 32);
        }
        $re = $re . "[$c,$x]";
    }
    $re = "^$re\$";
    if ($t =~ /$re/) {
        if ($t ne $p) {
            return 1;
        }
    }
    return 0;
}

# check if time representation matches a re.
sub _correct_time {
    my ($str) = @_;
    foreach my $re (@Meeting::time_re) {
        if ($str =~ /$re/) {
            return 1;
        }
    }
    return 0;
}

sub url_exists {
    my ($day, $h) = @_;
    if (exists($h->{'url'})) {
        return 1;
    } elsif (exists ($h->{$day})) {
        return 1;
    }
    return 0;
}

sub alias_check {
    my ($d) = @_;
    my $a = {};
    foreach my $name (keys %$d) {
        if (Meeting::is_meeting($name) && exists ($d->{$name}->{'aliases'})) {
            foreach my $alias (@{$d->{$name}->{'aliases'}}) {
                if (! exists($a->{$alias})) {
                    $a->{$alias} = [];
                }
                push (@{$a->{$alias}}, $name);
            }
        }
    }
    foreach my $alias (keys %$a) {
        if (scalar(@{$a->{$alias}}) > 1) {
            my $s = join (", ", @{$a->{$alias}});
            Messages::warn "alias '$alias' pointing to multiple meetings $s. This may lead to unpredictable behavior.";
        }
    }
}

# check the hash.
sub check {
    my ($d) = @_;
    foreach my $k (keys %{$d}) {
        foreach my $day (@Meeting::weekdays) {
            if (_caps($k, $day)) {
                Messages::warn "table name '$k' found. Likely supposed to be '$day'.";
            } elsif ($day eq $k) {
                # meeting name errors.
                foreach my $name (values %{$d->{$k}}) {
                    if (! exists($d->{$name})) {
                        Messages::warn "meeting name '$name' not found on day '$day'.";
                    } elsif (! Meeting::is_meeting($name)) {
                        Messages::warn "meeting name '$name' is a reserved name. on day '$day'.";
                    } else {
                        # check if URL exists.
                        if (! url_exists($day, $d->{$name})) {
                            Messages::warn "URL does not exist for meeting '$name' on day '$day'";
                        }
                    }
                }
                foreach my $timestr (keys %{$d->{$k}}) {
                    # check if time string is not correct.
                    if (! _correct_time($timestr)) {
                        Messages::warn "time string '$timestr' in day '$day' does not match any re.";
                    }
                }
            }
        }
        if (_caps($k, "Config")) {
            Messages::warn "table name '$k' found. Likely supposed to be 'Config'.";
        }
    }
    alias_check $d;
}


1;

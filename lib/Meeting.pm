#!/usr/bin/env perl

package Meeting;

use strict;
use warnings;
use diagnostics;
use File::Basename;
use lib dirname (__FILE__);
use Messages;

# All time regular expressions
our @time_re = (
    '(1[0-2]|[0-9])\s*:\s*[0-9][0-9]\s*(AM|PM)',
    '(1[0-2]|[0-9])\s*(AM|PM)'
);

# days of the week
our @weekdays = 
('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

our @not_meetings = (@weekdays, "Config");

# Parses time string and returns hours since midnight.
sub _time {
    my ($msg) = @_;
    if ($msg =~ /$time_re[0]/) {
        $msg =~ s/(?<=[0-9])(?=[A-Z])/ /g;
        my @a = split /[\s,:]+/, $msg;
        my ($hr, $min, $p) = @a;
        $hr = $hr%12;
        if ($p eq "PM") {
            $hr += 12;
        }
        return $hr + $min/60;
    } elsif ($msg =~ /$time_re[1]/) {
        $msg =~ s/(?<=[0-9])(?=[A-Z])/ /g;
        my @a = split /\s+/, $msg;
        my ($hr, $p) = @a;
        $hr %= 12;
        if ($p eq "PM") {
            $hr += 12;
        }
        return $hr;
    } else {
        Messages::error "$msg does not match any accepted regular expressions for time";
    }
}

# returns a default meeting to join as specified by the hash.
sub default_meeting {
    my ($d) = @_;
    my @t = localtime();
    my ( $min, $hour, $wday ) = ($t[1], $t[2], $t[6]);
    

    my %dayhash = ();
    if (exists($d->{$weekdays[$wday]})) {
        %dayhash = %{$d->{$weekdays[$wday]}};
    } else {
        print <<EOF;
Warning: No table has been found for day '$weekdays[$wday]'.
Note that days of the week must be capitalized properly.
EOF
    }
    
    my $res = "";
    foreach my $mt (sort { (_time $a) <=> (_time $b)} keys %dayhash) {
        my $t = _time $mt;
        if (($t - 5/60) < ($hour + $min/60)) {
            $res = $dayhash{$mt};
        }
    }
    return $res;
}

# check if entry is a meeting or not.
sub is_meeting {
    my ($k) = @_;
    foreach my $x (@not_meetings) {
        if ($x eq $k) {
            return 0;
        }
    }
    return 1;
}

# checks aliases and returns the name of the meeting it corresponds.
sub meeting_name {
    my ($d, $n) = @_;
    foreach my $k (keys %$d) {
        # dumb way to check if it is a meeting name or a configuration.
        if (is_meeting($k)) {
            my @a = ();
            if (exists($d->{$k}->{'aliases'})) {
                @a = @{$d->{$k}->{'aliases'}}
            }
            push @a, $k;
            foreach my $x (@a) {
                if ($x eq $n) {
                    return $k;
                }
            }
        }
    }
    Messages::error "no matching names or aliases found for '$n'";
}

# checks if operating system is macos.
sub _is_macos {
    return `uname` =~ /Darwin/;
}

# returns a system call to open meeting based on operating system
sub meeting_call {
    my ($d, $name) = @_;
    if (! $name) {
        print "No meeting to join to.\n";
        exit 0;
    }
    my $browser = $d->{'Config'}->{'browser'};
    my $cmd_string = $browser;

    my $url;
    my $wday = $weekdays[(localtime())[6]];
    if (exists ($d->{$name}->{$wday})) {
        $url = $d->{$name}->{$wday};
    }

    if (! exists($d->{$name})) {
        Messages::error "Meeting $name does not exist";
    } elsif (! exists($d->{$name}->{'url'}) and ! $url) {
        Messages::error "Meeting $name does not have a suitable url."
    } elsif (! $url) {
        $url = $d->{$name}->{'url'};
    }

    

    # macOS specific
    if (_is_macos) {
        if (! -d "/Applications/$browser.app" and ! -d "~/Applications/$browser.app") {
            Messages::warn "/Applications/$browser.app does not exist. Going with defaults.";
            return "open $url";
        }
        return "open -a '$browser' $url";
    }
    
    return "$cmd_string " . $url . " &disown";
}


1;

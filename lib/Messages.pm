#!/usr/bin/env perl

# Add the TOML Parsing stuff here.

package Messages;

use strict;
use warnings;
use diagnostics;
use feature 'say';

sub error {
    my ($msg) = @_;
    print "\e[1;31merror:\e[0m $msg\n";
    exit 1;
}

sub warn {
    my ($msg) = @_;
    print "\e[1;33mwarning:\e[0m $msg\n";
}

1;

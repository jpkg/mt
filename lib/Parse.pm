#!/usr/bin/env perl

# Add the TOML Parsing stuff here.

package Parse;

use strict;
use warnings;
use diagnostics;
use File::Basename;
use lib dirname (__FILE__);
use Messages;
use TOML qw(from_toml to_toml);
use File::Spec::Functions qw(catfile);
use feature 'say';

sub readfile {
    my ($file) = @_;
    my $fh;
    open($fh, "<", $file) or Messages::error "cannot open $file";
    my $res = "";
    while (<$fh>) {
        $res = $res . $_;
    }
    return $res;
}

sub parse_toml {
    my ($file) = @_;
    my $f = readfile $file;
    local $TOML::PARSER = TOML::Parser->new(
        inflate_boolean => sub { $_[0] eq 'true' ? 1 : 0 },
    );
    my ($res, $err) = TOML::from_toml $f;
    unless ($res) {
        Messages::error "TOML parsing error: $err";
    }
    return $res;
}

sub default_toml {
    my $srcdir = dirname (__FILE__);
    return readfile (catfile ($srcdir, "default.toml"));
}

1;

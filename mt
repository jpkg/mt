#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

use File::Basename;
use File::Spec::Functions qw(:ALL);
use lib (catfile ((dirname (__FILE__)), "lib"));
use Meeting;
use Parse;
use Messages;
use Check;

my $source_dir = File::Spec->rel2abs (dirname(__FILE__));
# config file.
my $config_file = catfile ($ENV{'HOME'}, ".config/mt/config.toml");
my $config_dir = dirname $config_file;


# writes configuration file.
sub gen_default {
    print "Writing default configuration file to $config_file...\n";
    system("mkdir -p $config_dir");
    my $fh;
    open($fh, ">>", $config_file) or die "$config_file cannot be opened.";
    my $x = Parse::default_toml();
    print $fh $x;
}

sub gen_env {
    print "Running build script...\n";
    system(catfile ($source_dir, "env", "build.pl"));
}


my ($config, $name, $call);

sub check_config {
# exit if no configuration file.
    if (! -f $config_file) {
        Messages::error ("no config file detected. Try running 'mt --gen' to build everything.")
    } else {
        return Parse::parse_toml ($config_file);
    }
}

if (exists($ARGV[0])) {
    if (! -f $config_file) {
        gen_default;
    }
    if ($ARGV[0] eq "--gen") {
        gen_env;
        exit;
    } elsif ($ARGV[0] eq "--check") {
        $config = check_config();
        Check::check($config);
        exit;
    } elsif ($ARGV[0] eq "--edit") {
        my $editor = ($ENV{'MT_EDITOR'} or 'vi');
        system("$editor $config_file");
        $config = check_config();
        Check::check($config);
        exit;
    }
    elsif ($ARGV[0] =~ /-+/) {
        Messages::error ("unknown option $ARGV[0]");
    }
}

$config = check_config;
if (exists($ARGV[0])) {
    $name = Meeting::meeting_name ($config,$ARGV[0]);
} else {
    $name = Meeting::default_meeting ($config);
}
$call = Meeting::meeting_call ($config,$name);
print ("$call 2>&1 > /dev/null\n");
system("$call 2>&1 > /dev/null");


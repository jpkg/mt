#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

use File::Basename;
use File::Spec::Functions;
use lib catfile ((dirname (__FILE__)),"..","lib");
use Meeting;
use Parse;

my $dict = {
    'Monday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt3',
    },
    'Tuesday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt3',
    },
    'Wednesday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt3',
    },
    'Thursday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt3',
    },
    'Friday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt3',
    },
    'Saturday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt3',
    },
    'Sunday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt3',
    },
    'Config' => {
        'browser' => 'brave',
    },
    'mt1' => {
        'url' => 'mt1.com',
        'aliases' => ['m1', '1']
    },
    'mt2' => {
        'url' => 'mt2.com',
        'aliases' => ['m2', '2']
    },
    'mt3' => {
        'url' => 'mt3.com',
        'aliases' => ['m3', '3']
    }
};

sub test1 {
    say "test1:";
    say "time is " . localtime();
    say "meeting name: " . Meeting::default_meeting $dict;
    say "meeting call: " . Meeting::meeting_call($dict, Meeting::default_meeting($dict));
}

sub test2 {
    say "test2:";
    my $name = Meeting::default_meeting $dict;
    say Meeting::meeting_call ($dict, $name);
}

sub test3 {
    say "test3: ";
    say "mt1: " . Meeting::meeting_name ($dict, "1");
    say "mt1: " . Meeting::meeting_call(Meeting::meeting_name ($dict, "1"));
    say "mt2: " . Meeting::meeting_name ($dict, "m2");
    say "mt2: " . Meeting::meeting_call(Meeting::meeting_name ($dict, "m2"));
    say "mt3: " . Meeting::meeting_name ($dict, "mt3");
    say "mt2: " . Meeting::meeting_call(Meeting::meeting_name ($dict, "mt3"));
}

test1;
print "\n";
test2;
print "\n";
test3;
print "\n";

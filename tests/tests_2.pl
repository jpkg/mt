#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

use File::Basename;
use File::Spec::Functions;
use lib catfile ((dirname (__FILE__)),"..","lib");
#use Meeting;
use Parse;
use Check;

# unit tests for checking errors.
my $dict = {
    'Monday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt3',
    },
    'Tuesday' => {
        "Notmatching" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt4',
    },
    'Wednesday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'nonexistent',
    },
    'Thursday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt4',
    },
    'Friday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt3',
    },
    'Saturday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt3',
    },
    'Sunday' => {
        "5AM" => 'mt1',
        "12PM" => 'mt2',
        "11:59PM" => 'mt3',
    },
    'Config' => {
        'browser' => 'brave',
    },
    'mt1' => {
        'url' => 'mt1.com',
        'aliases' => ['m1', '1']
    },
    'mt2' => {
        'url' => 'mt2.com',
        'aliases' => ['m2', '2']
    },
    'mt3' => {
        'url' => 'mt3.com',
        'aliases' => ['m3', '3']
    },
    'mt4' => {
        'Monday' => 'mt4.com',
        'Tuesday' => 'mt4.com',
        'aliases' => ['m4', '3']
    }
};

Check::check($dict);
